import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { EventsComponent } from './components/events/events.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { ModalModule } from 'ngx-bootstrap/modal';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { LanguageChangeService} from './services/language-change/language-change.service';
import { EventCreatorComponent } from './components/event-creator/event-creator.component';
import { LoginComponent } from './components/login/login.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AlertModule } from 'ngx-bootstrap/alert';
import { routing } from './app.routing';
import { AuthGuard } from './guards/auth-guard/auth-guard.component';
import { AuthenticationService } from './services/authentication/authentication.service';
import { UserService } from './services/user/user.service';
import { RegisterComponent } from './components/register/register.component';
import { HttpModule } from '@angular/http';
import { CKEditorComponent } from 'ng2-ckeditor';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { TimepickerModule } from 'ngx-bootstrap/timepicker';
import { FileUploadModule } from 'ng2-file-upload';
import { EventService } from './services/event/event.service';
import { EventDetailComponent } from './components/event-detail/event-detail.component';
import { AgmCoreModule } from '@agm/core';
import { EventOverviewComponent } from './components/event-overview/event-overview.component';

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}

@NgModule({
  declarations: [
    CKEditorComponent,
    AppComponent,
    HomeComponent,
    EventsComponent,
    NavbarComponent,
    EventCreatorComponent,
    LoginComponent,
    RegisterComponent,
    EventDetailComponent,
    EventOverviewComponent
  ],
  entryComponents: [
    LoginComponent,
    RegisterComponent
  ],
  imports: [
    routing,
    FileUploadModule,
    HttpModule,
    HttpClientModule,
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    BsDatepickerModule.forRoot(),
    TimepickerModule.forRoot(),
    ModalModule.forRoot(),
    AlertModule.forRoot(),
    BsDropdownModule.forRoot(),
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyAjprxDPt8iJBhehKxUkEv1-XoKCTSXwPI'
    }),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    })
  ],
  exports: [
    CKEditorComponent
  ],
  providers: [
    LanguageChangeService,

    AuthGuard,
    AuthenticationService,
    UserService,
    EventService,
    // providers used to create fake backend
    // fakeBackendProvider,
    // MockBackend,
    // BaseRequestOptions
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
