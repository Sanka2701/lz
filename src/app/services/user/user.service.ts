import { Injectable } from '@angular/core';
import {Http, Headers, RequestOptions, Response, RequestMethod} from '@angular/http';
import {User} from '../../models/user';
import {HttpClient, HttpHeaders} from '@angular/common/http';

@Injectable()
export class UserService {
  constructor(
    private _http: Http
  ) { }
  // constructor(private _http: HttpClient) { }

  URL = 'https://localhost:8443';

  create(user: User) {
    console.log('Registring user: ', user);

    const options = new RequestOptions();
    options.headers = this.buildHeaders();
    return this._http.post(this.URL + '/user/create', user, options).map((response: Response) => response.json());
  }
  // getAll() {
  //   return this._http.get(this.URL + '/api/users', this.jwt()).map((response: Response) => response.json());
  // }

  getById(id: number) {
    console.log('Retrieving user with ID: ', id);

    const options = new RequestOptions();
    options.headers = this.jwt();

    this._http.get(this.URL + '/user/' + id, options).map((response: Response) => response.json()).subscribe(
      data => {
        console.log('JE TO TU!');
      },
      err => {
        console.error('OOOOPS');
      }
    );
  }

  // update(user: User) {
  //   return this._http.put(this.URL + '/api/users/' + user.id, user, this.jwt()).map((response: Response) => response.json());
  // }
  //
  // delete(id: number) {
  //   return this._http.delete(this.URL + '/api/users/' + id, this.jwt()).map((response: Response) => response.json());
  // }

  // private helper methods

  private buildHeaders(): Headers {
    const headers = new Headers();
    headers.set('Content-Type', 'application/json');
    headers.set('Accept', 'application/json');
    return headers;
  }


  private jwt(headers?: Headers): Headers {
    if (!headers) {
      headers = this.buildHeaders();
    }

    // todo: move authToken to constants
    const token = localStorage.getItem('authToken');
    if (token) {
      headers.set('Authorization', 'Bearer ' + token);
    }

    return headers;
  }
}
