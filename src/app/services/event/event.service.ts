import { Injectable } from '@angular/core';
import { Event } from '../../models/event';
import {Http, Headers, RequestOptions, Response, RequestMethod} from '@angular/http';

@Injectable()
export class EventService {
  private URL = 'https://localhost:8443';

  constructor(
    private _http: Http
  ) { }

  save(event: Event) {
    return this._http.post(this.URL + '/event/create', event, this.buildHeaders())/*.map((response: Response) => response.json())*/;
  }

  getById(id: number) {
    console.log('Retrieving event with ID: ', id);

    // todo: remove subscribe
    this._http.get(this.URL + '/event/' + id, this.buildHeaders()).map((response: Response) => response.json()).subscribe(
      data => {
        console.log('Event with id: ' + id + ' loaded. Data: ', data);
      },
      err => {
        console.error('Event with id: ' + id + ' failed to load');
      }
    );
  }

  private buildHeaders(): RequestOptions {
    const token = localStorage.getItem('authToken');
    const headers = new Headers();
    headers.set('Content-Type', 'application/json');
    headers.set('Accept', 'application/json');
    headers.set('Authorization', 'Bearer ' + token);
    return new RequestOptions({ headers: headers });
  }
}
