import { TestBed, inject } from '@angular/core/testing';

import { LanguageChangeService } from './language-change.service';

describe('LanguageChangeService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [LanguageChangeService]
    });
  });

  it('should be created', inject([LanguageChangeService], (service: LanguageChangeService) => {
    expect(service).toBeTruthy();
  }));
});
