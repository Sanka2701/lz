import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Injectable()
export class LanguageChangeService {
  private languageSet: Language[]  = [
    {language: 'sk', flag: 'flag-icon-sk'},
    {language: 'en', flag: 'flag-icon-gb'}
  ];
  private currentLanguage: Language;


  constructor(private _translate: TranslateService) {
    this.switchLanguage('en');
  }

  switchLanguage(language: string) {
    this.currentLanguage = this.languageSet.find(lan => {
      return lan.language === language;
    });
    this._translate.use(this.currentLanguage.language);
  }
  getLanguage() {
    return this.currentLanguage.language;
  }
  getFlag() {
    return this.currentLanguage.flag;
  }
}

interface Language {
  language: string;
  flag: string;
}
