import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Router } from '@angular/router';

@Injectable()
export class AuthenticationService {
  constructor(
    private _http: Http,
    // private _http: HttpClient,
    private _router: Router
  ) { }

  private URL = 'https://localhost:8443';
  private TOKEN_PREFIX = 'Bearer ';
  private HEADER_STRING = 'Authorization';

  login(username: string, password: string) {
    const headers = new Headers();
    headers.set('Content-Type', 'application/json');
    headers.set('Accept', 'application/json');
    const options = new RequestOptions({ headers: headers , withCredentials: true });

    const credentials = { username: username, password: password };
    console.log ('sending credentials: ', credentials);

    return this._http.post(this.URL + '/user/authenticate', credentials, options)
      .map((response: Response) => {
        const user    = response.json();
        const token = response.headers.get(this.HEADER_STRING);

        if (user && token && token.startsWith(this.TOKEN_PREFIX)) {
          localStorage.setItem('currentUser', JSON.stringify(user));
          localStorage.setItem('authToken', token.replace(this.TOKEN_PREFIX, ''));
        }

        return user;
      });
  }

  logout() {
    localStorage.removeItem('currentUser');
    localStorage.removeItem('authToken');
    this._router.navigate(['/'], { queryParams: {}});
  }

  isLoggedIn() {
    return localStorage.getItem('currentUser') !== null;
  }
}
