import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { LoginComponent } from '../../components/login/login.component';

@Injectable()
export class AuthGuard implements CanActivate {
  bsModalRef: BsModalRef;

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    if (localStorage.getItem('currentUser')) {
      return true;
    }

    this.bsModalRef = this._modalService.show(LoginComponent);
    this.bsModalRef.content.messageArg = 'login.loginRequired';
    this.bsModalRef.content.returnUrl = state.url;

    return false;
  }

  constructor(private _modalService: BsModalService) { }
}
