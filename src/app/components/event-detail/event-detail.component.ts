import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MouseEvent, AgmMap } from '@agm/core';
import {Event} from '../../models/event';

@Component({
  selector: 'app-event-detail',
  templateUrl: './event-detail.component.html',
  styleUrls: ['./event-detail.component.css']
})
export class EventDetailComponent implements OnInit, OnDestroy {
  @ViewChild(AgmMap) private map: any;

  private sub: any;
  id: number;
  openedEvent: Event;


  markers: Marker[] = [
    {
      lat: 51.673858,
      lng: 7.815982,
      label: 'A',
      draggable: true
    },
    {
      lat: 51.373858,
      lng: 7.215982,
      label: 'B',
      draggable: false
    },
    {
      lat: 51.723858,
      lng: 7.895982,
      label: 'C',
      draggable: true
    }
  ];
  zoom = 8;
  lat = 51.673858;
  lng = 7.815982;

  clickedMarker(label: string, index: number) {
    console.log(`clicked the marker: ${label || index}`);
  }

  mapClicked($event: MouseEvent) {
    this.markers.push({
      lat: $event.coords.lat,
      lng: $event.coords.lng,
      draggable: true
    });
  }

  markerDragEnd(m: Marker, $event: MouseEvent) {
    console.log('dragEnd', m, $event);
  }

  constructor(private _route: ActivatedRoute) { }

  ngOnInit() {
    this.sub = this._route.params.subscribe(params => {
      this.id = +params['id'];
    });

    // todo: temporary passing of the data
    this.openedEvent = JSON.parse(localStorage.getItem('currentEvent'));
    console.log('Opened event', this.openedEvent);
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  private redrawMap() {
    this.map.triggerResize()
      .then(() => this.map._mapsWrapper.setCenter({lat: this.lat, lng: this.lng}));
  }

  toogleMap() {
    this.redrawMap();
  }

  getDateRange() {
    if (this.openedEvent.startDateTime && this.openedEvent.endDateTime) {
      return this.getDate(this.openedEvent.startDateTime) + ' - ' + this.getDate(this.openedEvent.endDateTime);
    } else {
      return this.getDate(this.openedEvent.startDateTime);
    }
  }

  // todo: consider different opening times on multi-day actions
  getTimeRange() {
    if (this.openedEvent.startDateTime && this.openedEvent.endDateTime) {
      return this.getTime(this.openedEvent.startDateTime) + ' - ' + this.getTime(this.openedEvent.endDateTime);
    } else {
      return this.getTime(this.openedEvent.startDateTime);
    }
  }

  getDate(dateTime: string) {
    const date = new Date(dateTime);
    const day   = date.getDate().toString();
    const month = (date.getMonth() + 1 ).toString();
    const year  = date.getFullYear().toString();

    return ('0' + day).slice (-2) + '.' + ('0' + month).slice (-2) + '.' + year;
  }

  getTime(dateTime: string) {
    const date = new Date(dateTime);
    const hours = date.getHours().toString();
    const minutes = date.getMinutes().toString();

    return ('0' + hours).slice (-2) + ':' + ('0' + minutes).slice (-2);
  }
}

interface Marker {
  lat: number;
  lng: number;
  label?: string;
  draggable: boolean;
}
