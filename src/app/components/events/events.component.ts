import { Component, OnInit } from '@angular/core';
import {Event} from '../../models/event';

@Component({
  selector: 'app-events',
  templateUrl: './events.component.html',
  styleUrls: ['./events.component.css']
})
export class EventsComponent implements OnInit {
  public loadedEvents: Event[] = [];
  private CHUNK_SIZE = 2;

  constructor() {
    // todo: temporary testinng data
    const e1 = new Event();
    e1.thumbnailUrl = 'http://www.mikulas.sk/filesII/informacne_centrum/kali_mikulas.jpg';
    e1.title = 'Koncert Kaliho v Liptovskom Mikulasi';
    e1.startDateTime = new Date('January 17, 2018 20:00:00');
    e1.endDateTime = new Date('January 17, 2018 22:00:00');
    e1.content = 'Predpredaj.sk vám umožňuje online nákup vstupeniek na rôzne hudobné,  športové a iné podujatia.';
    e1.id = 0;
    e1.userId = 1;

    const e2 = new Event();
    e2.thumbnailUrl = 'http://www.liptovzije.sk/wp-content/uploads/2017/12/LR-2018_registr%C3%A1cia-1024x470.jpg';
    e2.title = 'Registracia na Liptov Ride';
    e2.startDateTime = new Date('January 1, 2018 08:00:00');
    e2.endDateTime = new Date('January 5, 2018 23:00:00');
    e2.content = 'Športový rok 2018 začneme registráciou na tretí ročník štafetových pretekov Liptov Ride.';
    e2.id = 1;
    e2.userId = 1;

    this.loadedEvents.push(e1);
    this.loadedEvents.push(e2);
  }

  // todo: move to utilities as it is used across the app (ex.: event creator)
  public chunkEvents() {
    let i, j;
    const result = [];
    for ( i = 0, j = this.loadedEvents.length; i < j; i += this.CHUNK_SIZE) {
      result.push(this.loadedEvents.slice(i, i + this.CHUNK_SIZE));
    }

    return result;
  }

  ngOnInit() {
  }


}
