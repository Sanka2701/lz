import { AfterContentChecked, Component, Input, } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { AuthenticationService } from '../../services/authentication/authentication.service';
import { RegisterComponent } from '../register/register.component';
import { Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements AfterContentChecked   {
  model: any = {};
  loading = false;
  returnUrl: string;
  message: string;
  @Input() messageArg: string;

  constructor(
    private _router: Router,
    private _authenticationService: AuthenticationService,
    private _modalService: BsModalService,
    public _modalRef: BsModalRef
  ) {
    this.returnUrl = '/';
  }

  ngAfterContentChecked(): void {
    this.message = this.messageArg;
  }

  login() {
    this.loading = true;
    this._authenticationService.login(this.model.username, this.model.password)
      .subscribe(
        data => {
          console.log('frontend - login success, return url:', this.returnUrl);
          this._router.navigate([this.returnUrl], { queryParams: {}});
          this._modalRef.hide();
        },
        error => {
          console.log('frontend - login failure');
          this.message = 'login.invalidCredentials';
          this.loading = false;
        });
  }

  closeAndOpenRegister() {
    this._modalRef.hide();
    this._modalService.show(RegisterComponent);
  }
}
