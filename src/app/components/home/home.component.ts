import { Component, OnInit } from '@angular/core';
import {UserService} from '../../services/user/user.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(
    private _userService: UserService
  ) { }

  ngOnInit() {
  }


  public test() {
   var user = this._userService.getById(14);
  }
}
