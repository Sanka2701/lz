import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import {Event} from '../../models/event';
import {FormBuilder, FormControl, FormGroup} from '@angular/forms';
import {FileUploader, FileUploaderOptions} from "ng2-file-upload";
import {EventService} from "../../services/event/event.service";

@Component({
  selector: 'app-event-creator',
  templateUrl: './event-creator.component.html',
  styleUrls: ['./event-creator.component.css']
})
export class EventCreatorComponent implements OnInit {
  // public event: Event;
  myForm: FormGroup;
  public editorConfig = {
    language: 'en',
    toolbar: [
      ['Bold', 'Italic', 'Underline', 'StrikeThrough', '-', 'Undo', 'Redo', '-', 'Cut', 'Copy', 'Paste', 'Find', 'Replace', '-', 'Outdent', 'Indent'],
      '/',
      ['NumberedList', 'BulletedList', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'],
      ['Image', 'Link', 'Iframe', 'Source']
    ]
  };

   uploadedImgUrls: any[];
  private CHUNK_SIZE = 3;
  private SERVER_URL = 'https://localhost:8443';

  // public contentUploader: FileUploader = new FileUploader({url: this.SERVER_URL + '/up'});
  public contentUploader: FileUploader = new FileUploader({
    url: this.SERVER_URL + '/files/upload',
    removeAfterUpload: true,
    // allowedMimeType: ['image/jpg'],
    maxFileSize: 1024 * 1024,
    autoUpload: false,
    isHTML5: true,
  });

  public titleUploader: FileUploader = new FileUploader({
    url: this.SERVER_URL + '/files/upload',
    removeAfterUpload: true,
    // allowedMimeType: ['image/jpg'],
    maxFileSize: 1024 * 1024,
    autoUpload: false,
    isHTML5: true,
  });

  titleImgSet = false;
  titleImgUrl = '';

  constructor(
    private _formBuilder: FormBuilder,
    private _eventService: EventService
  ) { }

  ngOnInit() {
    this.uploadedImgUrls = [];
    this.myForm = this._formBuilder.group({
      startDate: [''],
      startTime: [''],
      endDate: [''],
      endTime: [''],
      title: [''],
      thumbnailUrl: [''],
      content: ['']
    });

    this.contentUploader.onCompleteItem = this.completeItem;
    this.titleUploader.onCompleteItem = this.completeTitleImg;

    console.log('myform value: ', this.myForm.value);
  }

  completeTitleImg = (item: any, response: any) => {
    console.log('title item: ', item);
    console.log('title response: ', response);

    const url = this.SERVER_URL + response;
    console.log('title url: ', url);

    this.titleImgSet = true;
    this.titleImgUrl = url;
    this.myForm.setValue({thumbnailUrl: url});

    console.log(this.myForm.get('thumbnailUrl'));
  };

  removeTitleImg = () => {
    this.titleImgSet = false;
    this.titleImgUrl = null;

    this.titleUploader.clearQueue();
    // todo: let the server know img was removed
  };

  completeItem = (item: any, response: any) => {
    console.log('item: ', item);
    console.log('response: ', response);

    this.uploadedImgUrls.push({url: this.SERVER_URL + response});
  };

  // todo: move to library
  chunkImgUrls = () => {
    let i, j, result = [];
    for ( i = 0, j = this.uploadedImgUrls.length; i < j; i += this.CHUNK_SIZE) {
      result.push(this.uploadedImgUrls.slice(i, i + this.CHUNK_SIZE));
    }

    return result;
  };

  onImgClick(imgUrl: string) {
    console.log('Adding img to content: ', imgUrl);

    let currentContent = this.myForm.get('content').value;
    currentContent += '<p style="text-align:center">' +
      '<img alt="" src=' + imgUrl + ' />' +
      '</p>';

    console.log('New content: ', currentContent);
    this.myForm.patchValue({content: currentContent});
  }

  getEventTest() {
    this._eventService.getById(12);
  }

  toDatetime(day: Date, time?: Date) {
    return new Date(Date.UTC(
      day.getFullYear(),
      day.getMonth(),
      day.getDate(),
      time ? time.getHours()  : 0,
      time ? time.getMinutes() : 0
    ));
  }

  onSubmit(form: any): void {
    const event: Event = new Event;

    event.id = null;
    event.userId = null;
    event.title = form.title;
    event.content = form.content;
    event.startDateTime = this.toDatetime(form.startDate, form.startTime).toISOString();
    event.endDateTime = this.toDatetime(form.endDate, form.endTime).toISOString();

    console.log('Submited event:', event);

    this._eventService.save(event).subscribe();
  }
}
