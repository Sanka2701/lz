import { Component, HostListener, Inject } from '@angular/core';
import { LanguageChangeService } from '../../services/language-change/language-change.service';
import { LoginComponent } from '../login/login.component';
import { BsModalService } from 'ngx-bootstrap/modal';
import { RegisterComponent } from '../register/register.component';
import { AuthenticationService } from '../../services/authentication/authentication.service';
import { DOCUMENT } from '@angular/common';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css'],
  providers: [LoginComponent]
})
export class NavbarComponent {
  public isFixed = false;
  public navBarHeight;

  constructor(
    @Inject(DOCUMENT) private _document: Document,
    private _languageChange: LanguageChangeService,
    private _modalService: BsModalService,
    private _authService: AuthenticationService
  ) {}

  @HostListener('window:scroll', [])
  onWindowScroll() {
    this.navbarAnchor();
    this.getNavBarHeight();
  }

  @HostListener('window:resize', [])
  onResize() {
    this.navbarAnchor();
    this.getNavBarHeight();
  }

  private navbarAnchor() {
    const imgElement = document.getElementById('lzTopPicture');
    const pictureHeight  = imgElement.getBoundingClientRect().height;
    const scrolledHeight = this._document.documentElement.scrollTop;

    if (scrolledHeight >= pictureHeight) {
      this.isFixed = true;
    } else if (this._document && scrolledHeight < pictureHeight) {
      this.isFixed = false;
    }
  }

  private getNavBarHeight() {
    const navBarElement  = document.getElementById('nav_bar');
    this.navBarHeight    = navBarElement.getBoundingClientRect().height;
  }

  public openLogin() {
    this._modalService.show(LoginComponent);
  }

  public isLoggedIn() {
    return this._authService.isLoggedIn();
  }

  public logout() {
    this._authService.logout();
  }

  public openRegister() {
    this._modalService.show(RegisterComponent);
  }

  public getLanguage() {
    return this._languageChange.getLanguage();
  }

  public getFlag() {
    return this._languageChange.getFlag();
  }

  public changeLanguage(language: string) {
    this._languageChange.switchLanguage(language);
  }
}
