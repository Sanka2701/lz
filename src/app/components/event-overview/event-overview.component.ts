import {Component, Input, OnInit} from '@angular/core';
import {Event} from '../../models/event';
import {Router} from '@angular/router';

@Component({
  selector: 'app-event-overview',
  templateUrl: './event-overview.component.html',
  styleUrls: ['./event-overview.component.css']
})
export class EventOverviewComponent implements OnInit {
  @Input() event: Event;

  constructor(private _router: Router) { }

  ngOnInit() {
  }

  goToEventDetails() {
    // todo : remove and pass value by code
    localStorage.setItem('currentEvent', JSON.stringify(this.event));
    this._router.navigate(['eventDetail', this.event.id]);
    return false;
  }
}
