import { Component } from '@angular/core';
import { Router } from '@angular/router';
import {UserService} from '../../services/user/user.service';
import {BsModalRef} from 'ngx-bootstrap';
import {User} from '../../models/user';


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent {
  model: User = new User;
  loading = false;

  constructor(
    private router: Router,
    private userService: UserService,
    public _modalRef: BsModalRef
  ) { }

  register() {
    this.loading = true;
    this.userService.create(this.model)
      .subscribe(
        data => {
          this.router.navigate(['/login']);
        },
        error => {
          this.loading = false;
        });
  }
}
