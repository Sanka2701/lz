import { Routes, RouterModule } from '@angular/router';
import {EventsComponent} from './components/events/events.component';
import {EventCreatorComponent} from './components/event-creator/event-creator.component';
import {HomeComponent} from './components/home/home.component';
import {AuthGuard} from './guards/auth-guard/auth-guard.component';
import {EventDetailComponent} from './components/event-detail/event-detail.component';

const appRoutes: Routes = [
  {
    path: '',
    component: HomeComponent
  },
  {
    path: 'events',
    component: EventsComponent
  },
  {
    path: 'createEvent',
    component: EventCreatorComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'eventDetail/:id',
    component: EventDetailComponent,
  }
];

export const routing = RouterModule.forRoot(appRoutes);
