/**
 * @license Copyright (c) 2003-2017, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
  config.removeDialogTabs = 'link:target;link:advanced;image:Link;image:advanced';
  // config.hideDialogFields="image:info:htmlPreview";
};

// CKEDITOR.on('dialogDefinition', function (ev) {
//   // Take the dialog name and its definition from the event data.
//   var dialogName = ev.data.name;
//   var dialogDefinition = ev.data.definition;
//   // Check if the definition is from the dialog we're
//   // interested in (the 'image' dialog).
//   if (dialogName == 'image') {
//     // Get a reference to the 'Image Info' tab.
//     var infoTab = dialogDefinition.getContents('info');
//     // Remove unnecessary widgets/elements from the 'Image Info' tab.
//     infoTab.remove('browse');
//     infoTab.remove('txtHSpace');
//     infoTab.remove('txtVSpace');
//     infoTab.remove('txtBorder');
//     infoTab.remove('txtAlt');
//     infoTab.remove('txtWidth');
//     infoTab.remove('txtHeight');
//     // infoTab.remove('htmlPreview');
//     infoTab.remove('cmbAlign');
//     infoTab.remove('ratioLock');
//   }
// });

CKEDITOR.on('dialogDefinition', function(ev) {
  var editor = ev.editor;
  var dialogName = ev.data.name;
  var dialogDefinition = ev.data.definition;

  if (dialogName == 'image') {
    var infoTab = dialogDefinition.getContents( 'info' );
    infoTab.remove( 'ratioLock' );
    infoTab.remove( 'cmbAlign' );
    infoTab.remove( 'txtBorder' );
    infoTab.remove( 'txtHSpace' );
    infoTab.remove( 'txtVSpace' );
    infoTab.remove( 'txtWidth' );
    infoTab.remove( 'txtHeight' );
    infoTab.remove('browse');
    infoTab.remove('htmlPreview');


    dialogDefinition.removeContents( 'Link' );
  }
});
